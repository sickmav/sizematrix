import React, { Component } from 'react';
import _ from 'lodash';

import TableCell from './TableCell';
import EditCell from './EditCell';
import UpdatingCell from './UpdatingCell';
import { editRow } from '../actions/settingsActions';
import { connect } from 'react-redux';
import { reduxForm} from 'redux-form';

@reduxForm({
  form: 'editSizes',
  enableReinitialize: true
})
@connect((store) => {
  return {
    editMatrix: store.settings.editMatrix,
    sizesChart: store.data.sizesChart
  }
})
export class TableRow extends Component {

  // renderRegion() {
  //   return (
  //     <TableCell
  //       key={'region' + this.props.row.SizeRegionId}
  //       id={this.props.row.SizeRegionId}
  //       regId={this.props.row.SizeRegionId}
  //
  //       value={this.props.row.SizeRegionName}
  //       editable={this.props.editableRow === this.props.row.SizeRegionId}
  //       onChange={(e, cellProps) => this.props.onChange(e, cellProps)}
  //     />
  //   );
  // }


  handleEditRow(e, r) {
    this.props.dispatch(editRow(r));
  }

  isEditable(i, j) {
    if (this.props.editMatrix) {
      if (this.props.editMatrix[i][j + 1] === 1) {
        return true;
      }
      return false
    }
    return false;
  }


  renderSizes() {

    let sizes,
    sizesList = [];

    // console.log(this.props.editRow);
    if (this.props.row) {
      sizes = [
        <td key={'erow-' + this.props.rowNo}>
          <EditCell
            handleEdit={(e) => this.handleEditRow(e, this.props.rowNo)} />
        </td>,
        // <td key={'bid-'+ (this.props.row.BrandId === null ? '01': this.props.row.BrandId)}>
        //   {this.props.row.BrandName === null ? 'General' : this.props.row.BrandName}
        // </td>,
        <td  key={'rid-' + this.props.row.SizeRegionId}>
          {this.props.row.SizeRegionName}
        </td>
      ];

      if (this.props.row.SizeList.length > 0) {
        // console.log(this.props.sizesChart[this.props.rowNo].SizeList[s.ColumnIndex + 1])

          sizesList = _.map(this.props.row.SizeList, (s, i) => (
            <TableCell
              key={'sid-'+ s.SizeId}
              id={s.SizeId}
              cind={s.ColumnIndex}
              type="text"
              value={s.Size}
              className="small-input"
              rowNo={this.props.rowNo}
              isEditable={this.isEditable(this.props.rowNo, s.ColumnIndex)}
            />
          ));
      }
      // console.log(this.props.sizesChart[this.props.rowNo].SizeList, this.props.row.SizeList);
      //
      // if (this.props.rowNo && this.props.sizesChart) {
      //   sizesList = this.props.sizesChart[this.props.rowNo].SizeList.map((s,i) => {
      //     // console.log(s.Size)
      //     // if (this.isEditable(this.props.rowNo, s.ColumnIndex)) {
      //       return (
      //         <td key={'sid-' + s.SizeId}>
      //           <Field
      //               value={this.props.sizesChart[this.props.rowNo].SizeList[s.ColumnIndex + 1]}
      //               name={'size-' + s.ColumnIndex + '-' + s.SizeId}
      //               type="text"
      //               component="input"/>
      //         </td>
      //       );
      //     // }
      //
      //     // return (
      //     //   <td key={'sid-' + s.SizeId}>
      //     //     <span>{s.Size}</span>
      //     //   </td>
      //     // )
      //   });
      // }


      // <TableCell
      // key={'sid-'+ s.SizeId}
      // id={s.SizeId}
      // cind={s.ColumnIndex}
      // type="text"
      // value={s.Size}
      // className="small-input"
      // rowNo={this.props.rowNo}
      // isEditable={this.isEditable(this.props.rowNo, s.ColumnIndex)}
      // />

    }

    sizes = sizes.concat(sizesList).concat([
        <td key={'upd' + this.props.rowNo}>
          <UpdatingCell
              index={this.props.rowNo}
              editOn="row"
          />
        </td>
      ]);

    return sizes;
  }

  // renderCells() {
  //   let cells = [],
  //     emc ;
  //
  //   if (this.props.row) {
  //     emc = this.props.row.map((e, i) => {
  //       return (
  //           <TableCell
  //               key={i}
  //               type="text"
  //               className="small-input"
  //               value={e}
  //               editing={this.props.editMatrix[i + 1]}
  //           />
  //       );
  //     });
  //
  //   }
  //   cells = cells.concat(emc)
  //
  //   return cells;
  // }

  render() {
    let cells = this.renderSizes();

    return (
      <tr>
        {cells}
      </tr>
    );
    // {this.renderCells()}
  }
}

export default TableRow;
