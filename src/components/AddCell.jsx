import React, { Component } from 'react';
import { Button } from 'react-materialize';
import { connect } from 'react-redux';
import _ from 'lodash';

@connect((store) => {
  return {
    editMatrix: store.settings.editMatrix,
    editing: store.settings.editing,
    saving: store.data.saving
  }
})
export default class AddCell extends Component {

  render() {
    const { editMatrix, saving, editing } = this.props;
    if (_.indexOf(editMatrix, 1) === -1 &&
        saving === false &&
        editing === false) {
      return (
        <Button
            className="btn"
            icon={"add"}
            waves="light"
            type="button"
            onClick={(e) => this.props.handleAdd(e)}
        />
      );
    }

    return (<span></span>);
  }

}
