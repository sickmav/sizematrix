import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
// import CustomInput from './CustomInput';

import AddCell from './AddCell';
import EditCell from './EditCell';
// import TableCell from './TableCell';
import UpdatingCell from './UpdatingCell';
import SelectCell from './SelectCell';
import { addNew, editRow, enableCellEdit } from '../actions/settingsActions';
import { addNewData, fetchRegions, setCellId, deleteRow } from '../actions/dataActions';
import { reduxForm, Field, getFormInitialValues, getFormValues} from 'redux-form';



const required = (value, allValues, props) => {
  if (value === null || value === '' || value === undefined) return 'Required'
};

// @reduxForm({
//   form: 'editSizes',
//   destroyOnUnmount: false,
//
//   // inputValidation
// })
@connect((store) => {
  return {
    editMatrix: store.settings.editMatrix,
    sizesChart: store.data.sizesChart,
    regions: store.data.regions,
    editingA: store.settings.editingA,
    editingIndex: store.settings.editingId,
    sizeTypeId: store.data.sizeTypeId,
    brandId: store.data.brandId,
    // initialValues: getFormInitialValues('editSizes')(store)
    // initialValues: mapInitialValues(store.data.sizesChart, store.settings.editingA, store.settings.editMatrix),


  };
})
export default class TableBody extends Component {

  componentWillMount() {
    this.props.dispatch(fetchRegions())
    // this.props.initialize(this.props.initialValues)
  }

  // renderInitialRow() {
  //
  //   if (this.props.rows.length === 0) {
  //     return (
  //       <TableRow
  //         brands={this.props.brands}
  //         row={this.props.emptyRow}
  //         editing='row'
  //         editingId={this.props.editingId}
  //         key={this.props.emptyRow.SizeRegionId}
  //       />
  //     );
  //
  //   } else {
  //     console.log('\n!!!! seems you have rows\n \n now render initial for existing rows\n');
  //   }
  //
  //   // TODO check ids & keys to be used
  // }

  handleAddRow(e) {
    this.props.dispatch(addNew('row'));
    this.props.dispatch(addNewData('row'));

  }

  shouldRenderAddCell() {
    const { sizesChart } = this.props;
    if (this.props.regions && sizesChart && sizesChart.length === this.props.regions.length) {
      return (<span></span>);
    }
    return ( <AddCell
         handleAdd={(e) => this.handleAddRow(e)}
     />);
  }

  renderLastRow() {

    const { sizesChart } = this.props;
    let lastRow = [
       <td key="add-row">
          {this.shouldRenderAddCell()}
       </td>,
       // <td key="brand-last"></td>,
       <td key="region-last"></td>
    ];



    if (sizesChart.length > 0) {
      // lastRow = [
      //    <td key="add-row">
      //      <AddCell
      //          handleAdd={(e) => this.handleAddRow(e)}
      //      />
      //    </td>,
      //    // <td key="brand-last"></td>,
      //    <td key="region-last"></td>
      // ];
      for (let i = 0; i < sizesChart[0].SizeList.length; i += 1) {
        lastRow.push(
          <td key={i}>
            <UpdatingCell
                index={i}
                editOn="column"
            />
          </td>
        );
      }
    }

    // if (this.props.editMatrix) {
    //   for(let i = 1; i < this.props.editMatrix.length; i += 1) {
    //     lastRow.push(
    //       <td key={i}>
    //         <UpdatingCell
    //             index={i - 1}
    //             editOn="column"
    //         />
    //       </td>
    //     )
    //   }
    // }

    lastRow.push(<td key="empty-last"></td>)

    return (
      <tr key="last-row">
        {lastRow}
      </tr>
    );
  }

  filterRegions () {
    const { sizesChart, regions, editingIndex } = this.props;
    let savedRegions;

    if (editingIndex >= 0){
      savedRegions = _.map(_.filter(sizesChart, (s, i) => { if (parseInt(editingIndex,10) !== i) return s.SizeRegionName;}), s => s.SizeRegionName);
    } else {
      savedRegions = _.map(sizesChart, s => s.SizeRegionName);

    }

    return _.filter(regions, r => _.indexOf(savedRegions, r.Name) < 0);
  }

  renderSizes(row, rowIndex) {

    let sizes,
    sizesList = [];
    const { sizesChart } = this.props;
    // console.log(this.props.editRow);
    // if (this.props.row) {
      sizes = [
        <td key={'erow-' + rowIndex}>
          <EditCell
              handleEdit={(e) => this.handleEditRow(e, rowIndex)}
              handleDelete={(e) => this.handleDeleteRow(e, rowIndex)}
          />
        </td>,
        // <td key={'bid-'+ (this.props.row.BrandId === null ? '01': this.props.row.BrandId)}>
        //   {this.props.row.BrandName === null ? 'General' : this.props.row.BrandName}
        // </td>,
      ];

      if (this.isEditable(rowIndex, 0) && this.props.editingA === 'row') {
        sizes.push(
          <td key={'rid-' + rowIndex}>
            <Field
                key={'rid-' + rowIndex}
                name="region"
                type="select"
                label="Select Region"
                className="select-input"
                onChange={this.onSelectedValue.bind(this)}
                options={this.filterRegions()}
                optionsType="regions"
                component={SelectCell}
                validate={required}
            />
          </td>
        );
                // value={row.SizeRegionId ? row.SizeRegionId: -1}
        // <SelectCell
        //     key={'rid-' + rowIndex}
        //     defaultValue={row.SizeRegionId ? row.SizeRegionId: -1}
        //     regions={this.filterRegions()}
        // />
        // validate={validate}
      } else {
        sizes.push(
          <td  key={'rid-' + row.SizeRegionId}>
            {row.SizeRegionName}
          </td>
        );
      }

      if (row.SizeList.length > 0) {

          sizesList = _.map(row.SizeList, (s, i) => {
            return this.renderTableCell(s, i, rowIndex)
          });
          // <TableCell
          //   key={'sid-'+ s.SizeId}
          //   id={s.SizeId}
          //   cind={s.ColumnIndex}
          //   type="text"
          //   value={s.Size}
          //   className="small-input"
          //   rowNo={index}
          //   isEditable={this.isEditable(index, s.ColumnIndex)}
          // />
      }
    // }

    sizes  = [
      ...sizes,
      ...sizesList,
      <td key={'upd' +rowIndex}>
        <UpdatingCell
            index={rowIndex}
            editOn="row"
        />
      </td>
    ];



    // = sizes.concat(sizesList).concat([
    //     <td key={'upd' +rowIndex}>
    //       <UpdatingCell
    //           index={rowIndex}
    //           editOn="row"
    //       />
    //     </td>
    //   ]);

    return sizes;
  }
  onSelectedValue(e) {
    // console.log('@onSelectedValue ', e.target.value);
  }
  // renderInput(field) {
  renderInput({ input, label, type, meta: { touched, error, warning}, rid, cid }) {
    return (
      <div className="input-field">
        <input {...input}
            placeholder={label}
            type={type}
            className='small-input editing-cell'
            key={rid + '-' + cid}
        />
        {touched &&
          ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
      </div>
    );
  }

  renderTableCell(s, i, rid) {

    if (this.isEditable(rid, i)) {


      // + '-' + s.SizeId
      return (
        <td key={i}>
          <Field key={'size-' + rid + '-' + i}
            name={'size-' + rid + '-'  + i}
            label={'size-' + rid + '-'  + i }
            type="text"
            rid={rid}
            cid={i}
            size={s}
            component={this.renderInput}
            validate={required}
          />
          <UpdatingCell
            index={rid}
            editOn="cell"/>
        </td>

      );
    }

    return (
      <td key={i} className="viewing-cell" onDoubleClick={e => this.enableCellEdit(e, rid, i, s)}>{s.Size}</td>
    )
  }

  enableCellEdit(e, rid, cid, s) {
    // console.log('enableCellEdit', rid, cid + 1);
    if (this.props.editingA === '' ) {
      this.props.dispatch(enableCellEdit(rid, cid + 1,this.props.initialValues))
      this.props.dispatch(setCellId(s.SizeId))
    }
  }

  handleEditRow(e, r) {
    this.props.dispatch(editRow(r));
  }
  handleDeleteRow(e, r) {
    // console.log('del row', r);
    const {sizeTypeId, brandId, sizesChart } = this.props;
    this.props.dispatch(deleteRow(sizeTypeId, sizesChart[r].SizeRegionId, brandId))
  }

  isEditable(i, j) {
    if (this.props.editMatrix.length > 0) {
      if (this.props.editMatrix[i][j + 1] === 1) {
        return true;
      }
      return false
    }
    return false;
  }

  renderRow(row, index) {
    return (
      <tr key={index}>
        {this.renderSizes(row, index)}
      </tr>
    );
  }

  renderTableRows() {
    const { sizesChart } = this.props;
    let rows = [];

    if (sizesChart.length > 0) {

      rows = _.map(sizesChart, (r,i) => (
        this.renderRow(r, i)
      ));
      // <TableRow key={i}
      // row={r} rowNo={i}
      // />
      // editRow={this.props.editMatrix[i]}
    }


    rows.push(this.renderLastRow())

    return rows;
  }

  render() {

    return (

      <tbody className="size-chart-row">
        {this.renderTableRows()}
      </tbody>

    );
    // {this.renderRows()}
  }
}
