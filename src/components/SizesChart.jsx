import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Form, getFormValues, SubmissionError } from 'redux-form';
import { Table, Card, Col, Row, Preloader } from 'react-materialize';
import _ from 'lodash';

import AddCell from './AddCell';
import EditCell from './EditCell';
import TableBody from './TableBody';

import { editColumn, addNew, disableEdit } from '../actions/settingsActions';
import { addNewData, saveColumn, saveRow, fetchChart, removeUnsavedData, updateSizeCell, updateSizes, deleteColumn } from '../actions/dataActions';

const mapInitialValues = (objWithValues, what, em, idx) => {
  let values = {}, line, column;

  if (what === 'cell') {
    for (let i=0; i<em.length; i+=1) {
      if (_.indexOf(em[i], 1)>0) {
        line = i;
        column = _.indexOf(em[i], 1) - 1;

        values['size-'+line+'-'+column] = objWithValues[line].SizeList[column].Size;//.editSizes

      }
    }
  } else if (what === 'row') {
    values['region'] = objWithValues[idx].SizeRegionId;
    for (let i=0; i<objWithValues[idx].SizeList.length; i += 1) {
      values['size-'+idx+'-'+i] = objWithValues[idx].SizeList[i].Size;
    }
  } else if (what === 'column') {
    for(let i=0; i<objWithValues.length; i+=1) {
	    values['size-'+i+'-'+idx] = objWithValues[i].SizeList[idx].Size;
    }
  }

  return values;
}


@reduxForm({
  form: 'editSizes',
  // enableReinitialize: true,
  // forceUnregisterOnUnmount: true,
  destroyOnUnmount: false,
  // validate
})
@connect((store) => {
  return {
    sizesChart: store.data.sizesChart,
    regions: store.data.regions,
    sizeTypeId: store.data.sizeTypeId,
    sizeTypeName: store.data.sizeTypeName,
    brandId: store.data.brandId,
    brandName: store.data.brandName,
    fetchingById: store.data.fetchingById,
    fetchedById: store.data.fetchedById,
    editingId: store.data.editingId,
    error: store.data.error,
    saving: store.data.saving,
    isNewData: store.data.isNewData,
    editIndex: store.settings.editingId,
    editMatrix: store.settings.editMatrix,
    editingA: store.settings.editingA,
    initialValues: mapInitialValues(store.data.sizesChart, store.settings.editingA, store.settings.editMatrix,store.settings.editingId),
    // values: getFormValues('editSizes')(store)
  };
})
export default class SizesChart extends Component {

  // componenWillMount() {
  //   if (_.keys(this.props.initialValues).length > 0) {
  //
  //     this.props.initialize(this.props.initialValues)
  //   }
  // }

  // componentWillReceiveProps(nextProps) {
  //   if (_.keys(nextProps.initialValues).length > _.keys(this.props.initialValues).length) {
  //     this.props.initialize(nextProps.initialValues);
  //   }
  // }
  componentDidUpdate(prevProps, prevState) {
    if (_.keys(this.props.initialValues).length > _.keys(prevProps.initialValues).length) {
      this.props.initialize(this.props.initialValues);
    }
  }

  handleSizesSubmit(values) {
    const { createRecord, reset,  editingA, sizeTypeId} = this.props;

    if (editingA === 'column') {
      let columnIndex, sizesChart, sizes;
      if (_.size(values) > 0) {
        columnIndex = parseInt(_.keys(values)[0].slice(7,9), 10);
        sizesChart = this.props.sizesChart;

        if (this.props.isNewData === true) {
          sizes = _.map(_.omit(values, ['region']), (r, i) => {
            return {
              Size: r,
              BrandID: this.props.brandId <= 0? null: this.props.brandId,
              SizeRegionId: sizesChart[i.slice(5,6)].SizeRegionId
              // SizeRegionID: sizesChart[i.slice(i.length - 1)].SizeRegionId
            };
          });
        } else { // editing column sizes
          sizes =_.map(_.omit(values, ['region']), (r, i) => {
            return {
              Size: r,
              SizeID: this.props.sizesChart[i.slice(5,6)].SizeList[i.slice(7,8)].SizeId
            };
          });
        }

      }
      if (this.props.isNewData === true) {

        this.props.dispatch(saveColumn(sizeTypeId, columnIndex, sizes, this.props.brandId))
      } else {
        this.props.dispatch(updateSizes(sizeTypeId, sizes, this.props.brandId, null))
      }
    }

    if (editingA === 'row') {
      let sizeRegionId, sizeRegionName, brandId, sizes, regionFound;
      if (_.size(values) > 0) {
        sizeRegionId = parseInt(values.region, 10);
        regionFound = _.find(this.props.regions, e => e.SizeRegionId === sizeRegionId);
        sizeRegionName = regionFound === undefined ? '': regionFound.Name;
        brandId = this.props.brandId <= 0? null: this.props.brandId;

        if (this.props.isNewData === true) {
          sizes = _.map(_.omit(values, ['region']), (r, i) => { return {Size: r, ColumnIndex: parseInt(i.slice(7, 8), 10)} })
        } else { // editing row sizes
          sizes = _.map(_.omit(values, ['region']), (r, i) => {
            return {
              Size: r,
              SizeID: this.props.sizesChart[this.props.editIndex].SizeList[i.slice(7,8)].SizeId
            }
          })
        }
      }

      if (this.props.isNewData === true) {

        this.props.dispatch(saveRow(sizeTypeId, sizeRegionId, sizes, brandId))
      } else {
        this.props.dispatch(updateSizes(sizeTypeId, sizes, brandId, sizeRegionId))
      }

    }

    if (editingA === 'cell') {
      let sizes = _.map(values, (v, i) => { return { Size: v, SizeID: this.props.editingId } })
      this.props.dispatch(updateSizeCell(sizeTypeId, ...sizes))

    }

    this.props.dispatch(disableEdit());
    this.props.dispatch(removeUnsavedData(editingA))
    // createRecord(values).then(() =>
     reset()
  //  );


  }

  renderTableHeader() {
    let head = [
      <th key="th1"></th>,
      // <th key="th2">Brand</th>,
      <th key="th3">Region</th>,
      <th key="th4">Sizes</th>
    ];

    if (this.props.editMatrix.length > 0) {
      for (let i = 0; i < this.props.editMatrix ; i += 1) {
        head.push(<th key={i}></th>)
      }
    }

    return head;
  }
  handleEditColumn(e, c) {

    this.props.dispatch(editColumn(c));
  }

  handleDeleteColumn(e, c) {
    const { sizeTypeId, brandId } = this.props;
    this.props.dispatch(deleteColumn(sizeTypeId, c, brandId))
  }

  handleAddColumn() {
    this.props.dispatch(addNew('column'))
    this.props.dispatch(addNewData('column'))

  }

  renderEditColumns() {
    let editCols = [
      <th key="th1">
        {this.renderLoader()}
      </th>,
      <th key="th3">Region/Sizes</th>,
      // <th key="th4">Sizes</th>
        // <th key="e3"></th>
    ];
    if (this.props.sizesChart.length > 0) {
      for (let i = 0; i < this.props.sizesChart[0].SizeList.length; i += 1) {
        editCols.push(
          <th key={i}>
            <EditCell
              handleEdit={(e) => this.handleEditColumn(e, i)}
              handleDelete={(e) => this.handleDeleteColumn(e, i)}/>
          </th>
        );
      }

      editCols.push(
        <th key='add-col'>
        <AddCell handleAdd={(e) => this.handleAddColumn(e)}/>
        </th>
      );
    }


    return editCols;
  }

  renderLoader() {
    if (this.props.saving === true) {
      return (<Preloader flashing size='small'/>);
    }
    return (<span></span>)
  }

  render() {
    const { handleSubmit, pristine, reset, submitting } = this.props;

    if (this.props.fetchedById === false && this.props.fetchingById === false) {
      return (<span></span>)
    }


    return (
      <Card>
        <Row>
          <Col s={12} className="sm-table">
            <Form onSubmit={handleSubmit(this.handleSizesSubmit.bind(this))}>
              <Table className="centered">
                <thead>
                  <tr className="size-chart-row">
                    {this.renderEditColumns()}
                  </tr>
                </thead>
                <TableBody/>
              </Table>
            </Form>
          </Col>
        </Row>
      </Card>
    );
    // editMatrix={this.props.editMatrix}
    // rows={this.props.sizesChart}
  }
}
