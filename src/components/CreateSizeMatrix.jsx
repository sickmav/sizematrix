import React, { Component } from 'react';
import { addNewSizeType } from '../actions/dataActions';
import { reduxForm, Field } from 'redux-form';
import { Input, Button, Card } from 'react-materialize';
import { connect } from 'react-redux';
import { setViewType } from '../actions/settingsActions';
import Notifications from 'react-notification-system-redux';

const validate = (values) => {
  const errors = {};

  if (!values.sizeTypeName) {
    errors.sizeTypeName = 'Required';
  }

  return errors;
}

@reduxForm({
  form: 'newSizeType',
  // enableReinitialize: true,
  // destroyOnUnmount: false,
  validate
})
@connect((store) => {
  return {
    form: store.form.newSizeType,
    notifications: store.notifications,
    sizeTypeId: store.data.sizeTypeId
  }
})
class CreateSizeMatrix extends Component {

  addNewSizeType() {
    this.props.dispatch(
      addNewSizeType(
        this.props.form.values.sizeTypeName,
        this.props.form.values.sizeTypeDescription
      )
    );


    setTimeout(() => {
      if (this.props.sizeTypeId > 0) {
        this.props.dispatch(setViewType('view'))
      }
    }, 3000);

    this.props.reset();
  }

  renderInput(field) {
    return (
      <Input {...field.input}
          type={field.type}
          label={field.label}
      />
    );
  }

  renderField(field) {
    return (
      <div className="input-field">
        <label>{field.input.placeholder}</label>
        <div>
          <input {...field.input}/>
          {field.error && field.touched && <span>{field.error}</span>}
        </div>
      </div>

    );
  }

  cancelCreateSizeMatrix(e, type) {
    this.props.dispatch(setViewType(type));
  }


  render() {
    const { handleSubmit, pristine, submitting} = this.props;

    // type="text"
    return (
      <Card className="create-size-matrix" >
        <form onSubmit={handleSubmit(this.addNewSizeType.bind(this))}>

          <div className="row">
            <div className="col s12">
              <p>Add a New Size Matrix</p>
            </div>
            <div className="col s4">
              <Field
                  name="sizeTypeName"
                  type="text"
                  label="Name"
                  placeholder="Name"
                  component={this.renderField}
              />
            </div>
            <div className="col s4">
              <Field
                  name="sizeTypeDescription"
                  label="Description"
                  type="text"
                  component={this.renderInput}
              />
            </div>
            <div className="col s4 btns">
              <Button
                  waves="light"
                  disabled={pristine || submitting}
                  type="submit"
              >
                Save
              </Button>
              <Button
                  waves="light"
                  type="button"
                  onClick={e => this.cancelCreateSizeMatrix(e, 'view')}
                  className="cancel"
              >
                Cancel
              </Button>
            </div>
          </div>
        </form>
        <Notifications notifications={this.props.notifications}/>
      </Card>
    );
  }
}


export default CreateSizeMatrix;
