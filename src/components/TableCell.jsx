import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';


@reduxForm({
  form: 'editSizes',
  enableReinitialize: true
})
@connect((store) => {
  return {
    editMatrix: store.settings.editMatrix,
    sizesChart: store.data.sizesChart
  }
})
export default class TableCell extends Component {

  renderInput(field) {
    // console.log('@renderInput',field);
    return (
      <input {...field.input}
          type={field.type}
          className={field.className}
      />
    );
    // value={field.value}
  //   <Input {...field.input}
  //   type={field.type}
  //   className={field.className}
  // defaultValue=''
  // value={field.value}
  // />
  }


  render() {
    // disabled={!this.props.editable}
  //   <Input
  //   type="text"
  //   browserDefault={false}
  //   placeholder="size"
  // defaultValue={this.props.value}
  // className="small-input"
  // />

    if (this.props.isEditable) {
      //   <Input
      //   type="text"
      //   browserDefault={false}
      //   placeholder="size"
      // defaultValue={this.props.value}
      // className="small-input"
      // />
      // console.log(this.props.sizesChart[this.props.rowNo].SizeList[this.props.cind + 1]);
      return (
        <td>
          <Field
            name={'size-' + this.props.cind + '-' + this.props.id}
            type="text"
            value={this.props.value}
            component={this.renderInput}
          />

        </td>

      );
      // value={this.props.sizesChart[this.props.rowNo].SizeList[this.props.cind + 1]}
      // component="input"
    }

    return (
      <td>{this.props.value}</td>
    );

    // type="text"
    // className="small-input"
    // value={this.props.value}
    // cind={this.props.cind}
    // component={this.renderInput}

    // <Field
    //   type="text"
    //   value={this.props.value}
    //   name={this.props.cind + '.' + this.props.id}
    //   key={this.props.cind + '.' + this.props.id}
    //   className="small-input"
    //   component={this.renderInput()}
    // />

  }
}
