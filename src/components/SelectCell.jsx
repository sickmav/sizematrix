import React, { Component } from 'react';
import _ from 'lodash';
import { Input } from 'react-materialize';
import { connect } from 'react-redux';


const required = value => (value? undefined: 'Required');

export default class SelectCell extends Component {

  componentWillMount() {
    // initial select value
    // const { regions } = this.props;
    // const initData = {
    //   'region': -1//regions[0].SizeRegionId
    // }
    // this.props.initialize(initData);
  }

  componentWillReceiveProps(props) {
    const { optionsType } = this.props;
  }

  renderOptions() {
    const { options, optionsType } = this.props;

    switch (optionsType) {
    case 'regions':
      return [
        ..._.map(options, r => (
            <option
                key={r.SizeRegionId}
                value={r.SizeRegionId}>
              {r.Name}
            </option>
          ))
      ];
    case 'sizeTypes':
      return [
        ..._.map(options, s => (
          <option
              key={s.SizeTypeId}
              value={s.SizeTypeId}
          >
            {s.Name}
          </option>
        ))
      ]
    case 'brands':
      return [
        ..._.map(options, b => (
          <option
            key={b.Value}
            value={b.Value}
          >
            {b.Text}
          </option>
        ))
      ]
    default:
      return [];
    }
  }



  // renderSelect({input, label, type, meta: { touched, error, warning } , className, renderFunction, name}) {
  //   // <Input {...input}
  //   // name={name}
  //   // type={type}
  //   // placeholder={label}
  //   // className={className}
  //   // >
  //   // {renderFunction}
  //   // </Input>
  //   return (
  //     <div>
  //       <Input {...input}
  //       name={name}
  //       type='select'
  //       className={className}
  //       >
  //         {renderFunction}
  //       </Input>
  //       {touched &&
  //         ((error && <span>{error}</span>) ||
  //         (warning && <span>{warning}</span>))}
  //     </div>
  //   );
  //   // <div className='input-field'>
  //   // <select label={label} {...input} className='dropdown-table-list'>
  //   // {renderFunction}
  //   // </select>
  //   // {touched &&
  //   //   ((error && <span>{error}</span>) ||
  //   //   (warning && <span>{warning}</span>))}
  //   //   </div>
  // }


  render() {//{this.renderSelect}
    const {input, label, meta: { touched, error, warning } , className, name} = this.props;
      return (
        <div>
          <Input {...input}
          name={name}
          type='select'
          placeholder={label}
          className={className}
          >
            {this.renderOptions()}
          </Input>
          {touched &&
            ((error && <span>{error}</span>) ||
            (warning && <span>{warning}</span>))}
        </div>
      );
  }
}
