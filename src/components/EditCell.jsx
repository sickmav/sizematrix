import React, { Component } from 'react';
import { Button, Modal } from 'react-materialize';
import { connect } from 'react-redux';
import _ from 'lodash';

@connect((store) => {
  return {
    editing: store.settings.editing
  }
})
export default class EditCell extends Component {

  // <Modal
  //     fixedFooter
  //     trigger={
  //       <Button
  //         className="btn btn-flat"
  //         id={this.props.id}
  //         icon={"mode_edit"}
  //         waves="light"
  //         type="button"
  //       />
  //     }
  //     actions={[
  //       <Button waves='light' modal='close' flat>No</Button>,
  //       <Button waves='light' modal='close'   flat onClick={(e) => this.props.handleEdit(e)}>Yes</Button>
  //     ]}
  // >
  //   The size you are trying to edit is attached to a live product.
  //   Are you sure you want to make any changes?
  // </Modal>



  render() {
    if (this.props.editing === false) {
      return (
        <div>

          <Button
              className="btn btn-flat"
              icon={'edit'}
              type="button"
              onClick={(e) => this.props.handleEdit(e)}
          />
          <Button
              className="btn btn-flat"
              icon={'delete'}
              type="button"
              onClick={(e) => this.props.handleDelete(e)}
          />
        </div>
      );
    }
    return (<span></span>);

  }
}
