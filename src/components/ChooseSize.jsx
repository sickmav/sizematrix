import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field, Form, formValueSelector } from 'redux-form';
import { Row , Col, Input, Button, Card, Preloader} from 'react-materialize';
import { BubbleLoader } from 'react-css-loaders';
import _ from 'lodash';
import Notifications from 'react-notification-system-redux';

import { fetchAllSizeTypes, setSizeType, getBrandsListAssignedToSizeType, setBrand, fetchChart, clearData } from '../actions/dataActions';
import { setViewType } from '../actions/settingsActions';

import SelectCell from './SelectCell';


const required = value => (value? undefined: 'Required');

@reduxForm({
  form: 'chooseSize',
  validate: required
  // enableReinitialize: true
})
@connect(
  (store) => {
    return {
      sizeTypes: store.data.allSizeTypes,
      brands: store.data.brands,
      fetchingSizes: store.data.fetchingAll,
      fetchedSizes: store.data.fetchedAll,
      fetchingBrands: store.data.fetchingBrands,
      fetchedBrands: store.data.fetchedBrands,
      sizeTypeId: store.data.sizeTypeId,
      brandId: store.data.brandId,
      notifications: store.notifications
      // selector: formValueSelector('chooseSize')
    };
  },
  // {
  //   pure: true
  // }
)
export default class ChooseSize extends Component {

  componentWillMount() {
    this.props.dispatch(fetchAllSizeTypes());
    if (this.props.sizeTypeId > 0) {
      this.props.dispatch(getBrandsListAssignedToSizeType(this.props.sizeTypeId));
    }
    const initData = {
      'sizeTypeId': this.props.sizeTypeId,
      'brandId': this.props.brandId
    }
    this.props.initialize(initData);
  }

  componentWillReceiveProps({ sizeTypeId }) {
    if (sizeTypeId !== this.props.sizeTypeId) {
      this.props.dispatch(setBrand(0, 'General'))
    }
  }

  // renderSizeTypeOptions() {
  //   let sizeTypeOptions = [];
  //   const { sizeTypes } = this.props;
  //
  //   if (this.props.fetchedSizes) {
  //     // _.orderBy(this.props.sizeTypes, ['SizeTypeId'], ['asc'])
  //     sizeTypeOptions = _.map(sizeTypes, (s, i) => {
  //       // _.findKey(s, ())
  //       return (
  //         <option
  //             key={s.SizeTypeId}
  //             value={s.SizeTypeId}
  //         >
  //           {s.Name}
  //         </option>)
  //     });
  //   }
  //
  //   sizeTypeOptions.unshift(<option key={-1} value="-1" disabled>Select Size Type</option>);
  //   return sizeTypeOptions;
  // }


  // renderBrandsOfSizeTypeOptions() {
  //   let brandsOptions = [];
  //   const { brands } = this.props;
  //   if (this.props.fetchedBrands) {
  //     brandsOptions = _.map(brands, (b, i) => {
  //       return (<option key={b.Value} value={b.Value}>{b.Text}</option>);
  //     })
  //   }
  //   else {
  //
  //     brandsOptions.unshift(<option key="-1" value="-1" disabled>Select Brand</option>);
  //   }
  //
  //
  //   return brandsOptions;
  // }

  onSelectedSizeType(e) {
    const { sizeTypes, sizeTypeId } = this.props;
    let selectedName = _.find(sizeTypes, el =>  el.SizeTypeId === parseInt(e.target.value, 10)).Name;
    this.props.dispatch(setSizeType(parseInt(e.target.value, 10), selectedName));
    // if (this.props.brands.length > 0) {

    // } else {
      this.props.dispatch(getBrandsListAssignedToSizeType(sizeTypeId));
      // this.props.dispatch(setBrand(0, 'General'))

    // }
  }

  onSelectedBrand(e) {
    if (e.target.value >= 0) {
      let selectedName = _.find(this.props.brands, el =>  el.Value === e.target.value).Text;
      this.props.dispatch(setBrand(parseInt(e.target.value, 10), selectedName));
    }
  }

  submit() {
    // let not = Notification.newInstance({ prefixCls: 'toast-container'});

    if (this.props.sizeTypeId > 0) {
      this.props.dispatch(fetchChart(
        this.props.sizeTypeId,
        (this.props.brandId > 0)? this.props.brandId: 0
      ));

    } else {
      this.props.dispatch(Notifications.warning({
        message: 'Please select a size type first.',
        position: 'tr',
        autoDismiss: 3,
        dismissible: false
      }))

    }
  }

  renderSelect(field) {
    return (
      <Input {...field.input}
          name={field.name}
          type={field.type}
          label={field.label}
          className={field.className}
          >
          {field.renderFunction}
      </Input>
      // value={field.value}
      // placeholder={field.placeholder}
    );
  }

  handleOperation(e, type) {
    this.props.dispatch(setViewType(type))
    this.props.dispatch(clearData())
  }

  render() {

    const { handleSubmit, sizeTypes, brands, sizeTypeId, brandId
      // , pristine, submitting
    } = this.props;
    // const selector = formValueSelector('chooseSize');

    if (!this.props.fetchedSizes) {
      return (<BubbleLoader color="#03045" duration={2} size={6}/>);
    }


    // <Select
    //     key="size-type"
    //     name="sizeType"
    //     label="Size Chart"
    //     placeholder="Select Size Type"
    //     defaultValue={this.props.sizeTypeId}
    //     options={this.props.sizeTypes}
    //     fetched={this.props.fetchedSizes}
    // />
    return (
      <Card >
        <Row>
          <Col s={12}>
            <Form onSubmit={handleSubmit(this.submit.bind(this))}>
            <Row>
              <Col s={4}>
              <Field
                  key="size-type"
                  name="sizeType"
                  label="Select Size Type"
                  type="select"
                  className="select-input"
                  onChange={this.onSelectedSizeType.bind(this)}
                  component={SelectCell}
                  options={sizeTypes}
                  optionsType="sizeTypes"
                  value={sizeTypeId}
                  validate={required}
              />
              </Col>
              <Col s={4}>
                <Field
                    key="brand"
                    name="brand"
                    label="Select Brand"
                    className="select-input"
                    type="select"
                    onChange={this.onSelectedBrand.bind(this)}
                    component={SelectCell}
                    optionsType="brands"
                    options={brands}
                    value={brandId}
                />
              </Col>
              <Col s={4} className="view-add-new">
                <Button className="btn btn-input"
                    waves="light"
                    type="submit">
                  View
                </Button>
                <span>or</span>
                <Button
                    className="btn btn-sizes"
                    waves="light"
                    type="button"
                    onClick={e => this.handleOperation(e, 'new')}>
                  Add New
                </Button>
              </Col>
              <Col s={12} className="choose-size"> </Col>
            </Row>
            </Form>
            <Notifications notifications={this.props.notifications}/>
          </Col>
        </Row>
      </Card>
    );
  }
}
