import React, { Component } from 'react';
import { Button } from 'react-materialize';
import { disableEdit, disableEditAndRemove } from '../actions/settingsActions';
import {  removeUnsavedData } from '../actions/dataActions';
import { submit } from 'redux-form';

import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';

// @reduxForm({
//   form: 'editSizes',
//   enableReinitialize: true
// })
@connect((store) => {
  return {
    editing: store.settings.editing,
    editingId: store.settings.editingId,
    editingA: store.settings.editingA,
    isNewData: store.data.isNewData
  };
})
export default class UpdatingCell extends Component {

  disableEdit() {
    if (this.props.isNewData === true) {
      this.props.dispatch(removeUnsavedData(this.props.editOn));
      this.props.dispatch(disableEditAndRemove(this.props.editingA))
    } else {

      this.props.dispatch(disableEdit());
    }
  }
  save(e) {

    this.props.dispatch(submit('editSizes'));
  }

  shouldBeVisible() {
    if (this.props.editing === true &&
        this.props.editingId === this.props.index
        && this.props.editingA === this.props.editOn) {
       return true;
    } else if (this.props.editing === true &&
      this.props.editingA === this.props.editOn &&
      this.props.editingA === 'cell') {
        return true;
      }

    return false;
  }

  render() {

    const { pristine, submitting } = this.props;

    if (this.shouldBeVisible() === false) {
      return (<span></span>);
    }
    return (
      <div className="updating-cell">
        <Button
            className="btn btn-input"
            id={this.props.id}
            icon={"save"}
            waves="light"
            type="button"
            onClick={this.save.bind(this)}
            disabled={submitting}
        />
        <Button
            className="btn btn-flat btn-input"
            id={this.props.id}
            waves="teal"
            icon={"clear"}
            type="button"
            onClick={this.disableEdit.bind(this)}
            disabled={submitting}
        />
      </div>

    );
  }
}
