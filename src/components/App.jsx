import React, { Component } from 'react';

import ChooseSize from './ChooseSize';
import SizesChart from './SizesChart';

import { Row, Col } from 'react-materialize';
import CreateSizeMatrix from './CreateSizeMatrix';
import { connect } from 'react-redux';



export class App extends Component {

  render() {
    if (this.props.viewType === 'new') {
      return (
        <Row>
          <Col s={12}>
            <CreateSizeMatrix />
          </Col>
        </Row>
      )
    } else if (this.props.viewType === 'view') {
      return (
        <Row>
          <Col s={12}>
            <ChooseSize />
          </Col>
          <Col s={12}>
            <SizesChart />
          </Col>
        </Row>
      );
    }

    return (<span>none yet</span>);
  }
}


export default connect((store) => {
  return {
    viewType: store.settings.viewType
  }
})(App);

//
// <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
// <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css"/>
// <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
// <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
