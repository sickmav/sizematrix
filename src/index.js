import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import './styles/index.scss';

import { Provider } from 'react-redux';
import store from './store';

const hostNode = document.getElementById('root-sizes-chart')

if (hostNode !== null) {

  ReactDOM.render(
    <Provider store={store}>
    <App />
    </Provider>,
    hostNode
  );
}
