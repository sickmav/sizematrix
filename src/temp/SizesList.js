import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { BubbleLoader } from 'react-css-loaders';

import { fetchById, fetchAllSizeTypes } from '../actions/dataActions';

@connect((store) => {
  return {
    data: store.data.allSizeTypes,
    fetching: store.data.fetchingAll,
    fetched: store.data.fetchedAll
  }
})
export default class SizesList extends Component {

  componentWillMount() {
    this.props.dispatch(fetchAllSizeTypes());
  }


  hoverRowHandle(e) {
    this.props.dispatch(fetchById(e.currentTarget.id))
  }

  renderAllSizeTypes() {
    let r;
    if (this.props.data) {
      r = _.map(this.props.data, (s) => {
        return (
          <tr
              key={s.SizeTypeId}
              id={s.SizeTypeId}
              onClick={this.hoverRowHandle.bind(this)}
          >
            <td>{s.Name}</td>
            <td>{s.Description}</td>
            <td></td>
          </tr>
        );
      });
    }

    return r;
  }

  render() {

    if (!this.props.fetched) {
      return (<BubbleLoader color="#03045" duration={2} size={6}/>);
    }

    return (
      <table className="highlight">
        <thead>
          <tr>
            <td>Name</td>
            <td>description</td>
            <td></td>
          </tr>
        </thead>
        <tbody>
          {this.renderAllSizeTypes()}
        </tbody>
      </table>
    );
  }
}
