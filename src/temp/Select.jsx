import React, { PureComponent } from 'react';
import { Field } from 'redux-form';
import _ from 'lodash';
import { connect } from 'react-redux';

import { Input } from 'react-materialize'// or use html

import { setSizeType, getBrandsListAssignedToSizeType } from '../actions/dataActions'
// import { fetchAllSizeTypes } from '../actions/dataActions';

@connect()
export default class Select extends PureComponent {


  // componentWillMount() {
  //   this.props.dispatch(fetchAllSizeTypes());
  // }

  onSelectedSizeType(e) {
    let selectedName = _.find(this.props.options, el =>  el.SizeTypeId === parseInt(e.target.value, 10)).Name;
    this.props.dispatch(setSizeType(parseInt(e.target.value, 10), selectedName));
    // // if (this.props.brands.length > 0) {
    //
    // // } else {
      this.props.dispatch(getBrandsListAssignedToSizeType(parseInt(e.target.value, 10)));
    //   this.props.dispatch(setBrand(0, 'General'))
    //
    // // }
  }


  renderSizeTypeOptions() {
    let sizeTypeOptions = [];

    if (this.props.fetched) {
      // _.orderBy(this.props.sizeTypes, ['SizeTypeId'], ['asc'])
      sizeTypeOptions = _.map(this.props.options, (s, i) => {
        return (
          <option
              key={s.SizeTypeId}
              value={s.SizeTypeId}
          >
            {s.Name}
          </option>)
      });
    }

    sizeTypeOptions.unshift(<option key={-1} value="-1" disabled>Select Size Type</option>);
    return sizeTypeOptions;
  }

  renderSelect(field) {
    return (
      <Input {...field.input}
          name={field.name}
          type={field.type}
          label={field.label}
          className={field.className}
          >
          {field.renderFunction}
      </Input>
      // value={field.value}
      // placeholder={field.placeholder}
    );
  }

  render() {
    return (
      <Field
        name={this.props.name}
        label={this.props.label}
        placeholder={this.props.placeholder}
        type="select"
        className="select-input"
        onChange={this.onSelectedSizeType.bind(this)}
        component={this.renderSelect.bind(this)}
        options={this.props.options}
        renderFunction={this.renderSizeTypeOptions()}
        value={this.props.defaultValue}
      />
    );
  }
}
