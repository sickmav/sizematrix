import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';

const validate = values => {
  const errors = {};
  return errors;
}

export class CustomInput extends Component {
  componentWillMount() {
    const { rid, cid, size } = this.props;
    const  key = 'size-' + rid + '-' + cid + '-' + size.SizeId
    let  initValue = {};
    initValue[key] = size.Size;
    this.props.initialize(initValue);
  }

  renderInput(field) {
    return (
      <div>
        <input {...field.input}
          type="text"
            className='small-input editing-cell'
        />
        {field.error && field.touched && <span>{field.error}</span>}
      </div>
    );
    // type={type}
    // key={this.props.rid + '-' + props.cid}
  }

  render() {
    // const { input: { value, onChange } } = this.props;
    // return (
    //   <input
    //     type='text'
    //     className='small-input editing-cell'
    //   />
    // );
    const { name, rid, cid, size } = this.props;
    return (
      <Field
          name={name}
          key={name}
          rid={rid}
          cid={cid}
          size={size}
          component={this.renderInput}
      />
    )
  }
}

export default reduxForm({
  form: 'editSizes',
  validate
})(CustomInput);
