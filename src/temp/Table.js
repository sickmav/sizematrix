import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BubbleLoader } from 'react-css-loaders';

import TableHeader from './TableHeader';

import TableBody from './TableBody';

import { fetchById, save } from '../actions/dataActions';

@reduxForm({
  form: 'sizeMatrix'
})
@connect((store) => {
  return {
    // data: store.data.sizesChart,


    fetching: store.data.fetchingAll,
    fetched: store.data.fetchedAll,

    // editMatrix: store.settings.editMatrix,

    editing: store.data.editing,
    editingId: store.data.editingId
  }
})
export default class Table extends Component {

  componentWillMount() {
    this.props.dispatch(fetchById(this.props.sizeTypeId));
  }

  save() {
    this.props.dispatch(save());
  }


  render() {

    const { handleSubmit } = this.props;

    if (!this.props.fetched) {
      return (
        <BubbleLoader color="#eed" duration={3} size={5}/>
      );
    }

    log('@table ', this.props.data);

    return (
      <form onSubmit={handleSubmit(this.save.bind(this))}>
        <table className="centered">
          <TableHeader
          />
          <TableBody
            rows={this.props.data}
            
            editing={this.props.editing}
            editingId={this.props.editingId}
          />
        </table>
      </form>
    );

  }
}
