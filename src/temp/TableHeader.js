import React, { Component } from 'react'
import EditCell from './EditCell'
import { Button } from 'react-materialize'
import { connect } from 'react-redux';

@connect((store) => {
  return {
    columns: store.data.data,
    emptyRow: store.data.emptyRow
  }
})
export default class TableHeader extends Component {

  renderTableHeads() {
    // let countHeaders = 4;
    let headers = [];

    if (this.props.columns && this.props.columns.length > 0) {
      console.log(this.props.columns);
    }
    return headers;
  }

  render() {

    let headersCount = 4;
    let headers = [];


    if (this.props.columns && this.props.columns.length > 0 ) {
      console.log('@TABLEHEADER', this.props.columns);
      headersCount += this.props.columns[0].SizeList.length;

      headers.push(<th key={'editcol--1'}>{this.props.typeName}</th>)

      for (let i = 0; i < headersCount; i += 1) {
        if (i < 2) {
          headers.push(<th key={'editcol-' + i}></th>);

        } else if (i === headersCount - 1) {
          headers.push(<th key={'addcol-' + i}>
            <Button
                className="btn btn-flat"
                id={this.props.id}
                icon="add"
                waves="light"
            ></Button>
          </th>)
        } else {

          headers.push(
            <th key={'editcol-' + i}>
              <EditCell handleEdit={(e) => this.props.handleEditColumn(e)}/>
            </th>
          )
        }
      }

    }
    //else {
      // for (let i = 0; i < initialColumns; i += 1) {
      //   if (i === initialColumns - 1) {
      //     headers.push(<th key={'col' + i}>
      //       <Button
      //           className="btn btn-flat"
      //           icon="add"
      //           waves="light"
      //           onClick={() => this.props.addColumn('addColumn')}
      //       ></Button>
      //     </th>)
      //   } else {
      //     headers.push(<td key={'col' + i}></td>);
      //   }
      // }
    //}

    return (
      <thead>
        <tr>
          {this.renderTableHeads()}
        </tr>

      </thead>
    );
    // {headers}
  }
}
