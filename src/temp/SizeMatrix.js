import React, { Component } from 'react';
import Table from './Table';
import CreateSizeMatrix from './CreateSizeMatrix';
import { connect } from 'react-redux';
import { Row, Col } from 'react-materialize';

@connect((store) => {
  return {

    sizeTypeId: store.data.sizeTypeId,
    sizeTypeName: store.data.sizeTypeName,
    sizeTypeDescription: store.data.sizeTypeDescription,

    editing: store.data.editing,
    editingId: store.data.editingId,

    editableRow: store.settings.editableRow,
    editableWhat: store.settings.editableWhat,
    editable: store.settings.editable
  };
})
export default class SizeMatrix extends Component{


  //   // TODO check which handlers need more binding
  //   this.handleChangeCreate = this.handleChangeCreate.bind(this);
  //   this.handleSubmitCreate = this.handleSubmitCreate.bind(this);
  //   this.handleChangeSelect = this.handleChangeSelect.bind(this);




  handleChangeCreate(e) {
    //
    // const name = e.target.name;
    // this.setState({
    //   [name]: e.target.value
    // });
  }

  handleSubmitCreate(e) {
    // console.log('submit ev ', e);
    // e.preventDefault();
    //
    //
    // fetch('http://localhost:85/Sizes/AddNewSizeType', {
    //   method: 'POST',
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json'
    //   },
    //   mode: 'cors',
    //   body: JSON.stringify({
    //     sizeTypeName: this.state.sizeTypeName,
    //     sizeTypeDescription: this.state.sizeTypeDescription
    //   })
    // })
    // .then(res => res.json())
    // .then(res =>  {
    //   console.log('response for AddNewSizeType ', res);
    //   if (res.success && res.success > -1) {
    //     this.setState({
    //       sizeTypeSaved: true,
    //       sizeTypeId: res.success
    //     });
    //   }
    // });
  }

  createEmptyRow() {
    // const data = this.state.data.slice();
    // const lastRow = data[data.length - 1];
    // const sizesLength = lastRow.SizeList.length;
    // const lastIndex = lastRow.SizeRegionId;
    // console.log(lastIndex);
    // if (lastRow.SizeRegionId > 0) {
    //   data.push({
    //     SizeRegionName: '',
    //     SizeRegionId: 0,
    //     SizeList: new Array(sizesLength).fill({ // todo unique SizeId ?
    //       SizeId: -1,
    //       Size: '',
    //       ColumnIndex: ''
    //     })
    //   });
    //
    //   this.setState({
    //     data,
    //     editableWhat: 'row',
    //     editableRow: -1
    //   });
    // } else {
    //   console.log('@createEmptyRow 1 exists already');
    // }
  }

  createEmptyColumn() {
    // const data = this.state.data.slice();
    // console.log('@createEmptyColumn');
    // // TODO hide addColumn btn when inserting it
    // data.forEach((r,i) => {
    //   r.SizeList.push({
    //     SizeId: -i,
    //     Size: '',
    //     ColumnIndex: r.SizeList.length + 1
    //   });
    // });
    //
    // this.setState({data});
  }

  handleThat(twat) {
    console.log('handle that twat');
    switch(twat) {
      case 'addRow':
          this.createEmptyRow();
        break;
      case 'removeRow':
          this.removeLastRow();
        break;
      case 'addColumn':
        this.createEmptyColumn();
        break;
      case 'removeColumn':
        this.removeLastColumn();
        break;
      default:
        break;
    }
  }

  handleCellChange(e, cellProps) { // TODO: something really wrong here to fix

    const data = this.state.data.slice();

    if (cellProps && cellProps.cind) {

      for (let i = 0; i < data.length; i += 1) {
        for (let j = 0; j < data[i].SizeList.length; j += 1) {
          if (cellProps.cind === data[i].SizeList[j].ColumnIndex &&
              cellProps.id === data[i].SizeList[j].SizeId) {

            data[i].SizeList[j].Size = e.target.value;
          }
        }
      }
    } else {

      for(let i = 0; i < data.length; i += 1) {
        if (cellProps.id === data[i].SizeRegionId) {

          data[i].SizeRegionName = e.target.value;
        }
      }
    }

    this.setState({ data });
  }

  handleChangeSelect(e) {
    console.log('select changed ', e);
  }

  handleEditRow(e) {

    // TODO some bug here
    // console.log('toggle edit from ', e.target.id, typeof e.target.id, e.target, 'for ', this.state.editableRow, typeof this.state.editableRow);
    if (this.state.editableRow === Number(e.target.id)) {
      // this.setState({
      //   editableRow: -1
      // });
    } else {

      // this.setState({
      //   // editableRow: !this.state
      //   editableRow: Number(e.target.id)
      // })
    }
  }

  handleSaveRow(e) {
    console.log('call api', e, e.target);
    let rowToUpdate = this.state.data.find((el) => {
      return el.SizeRegionId === Number(e.target.id);
    });
    // console.log(eee);

    fetch('http://localhost:85/Sizes/AddNewRow', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      mode: 'cors',
      body: JSON.stringify({
        sizeTypeId: this.state.sizeTypeId,
        sizeRegionId: rowToUpdate.SizeRegionId,
        sizes: rowToUpdate.SizeList,
        sizeRegionName: rowToUpdate.SizeRegionName,
        brandId: null
      })
    }).then(res => res.json())
      .then(res => {
        console.log('response for UpdateSize ', res);
        this.setState({
          editableRow: -1
        });
      })
      .catch(err => {
        console.log('update size error ', err);
      });

  }

  handleEditColumn(e) {
    console.log('@handleEditColumn ', e);

    // TODO toggle editing on the specified size column
  }

  handleSaveColumn(e) {
    console.log('@handleSaveColumn ', e);
    // TODO API call to save column
  }

  render() {

    if (this.props.sizeTypeId === null) {
      return (
        <CreateSizeMatrix
            sizeTypeName={this.props.sizeTypeName}
            sizeTypeDescription={this.props.sizeTypeDescription}
        />
      );
    }

    // data={this.props.data}
    return (
      <Row>
        <Col s={12} className="sm-container">
          <Table
              sizeTypeId={this.props.sizeTypeId}
              editing={this.props.editing}



              editableRow={this.props.editableRow}
              handleThat={(twat) => this.handleThat(twat)}
              handleCellChange={(e, cellProps) => this.handleCellChange(e, cellProps)}
              onChangeSelect={(e) => this.handleChangeSelect(e)}
              handleEditRow={(e) => this.handleEditRow(e)}
              handleSaveRow={(e) => this.handleSaveRow(e)}
              handleEditColumn={(e) => this.handleEditColumn(e)}
          />
        </Col>
      </Row>
    );
  }
}
