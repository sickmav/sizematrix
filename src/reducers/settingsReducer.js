import _ from 'lodash';

const init = {

  editMatrix: [],
  editing: false, // or 'row' or 'column',
  editingId: null, // or columnId or lineId
  editingA: '',

  saved: null,

  fetching: false, // show a loader on true
  fetched: false, // will now show if empty


  error: null,
  viewType: 'view',//'new',//null,
  // bla
  editableRow: -1,
  editableWhat: null,
  editable: false,
};


const nullify = (matrix) => {
  return _.map(matrix, a => {
    return _.map(a, e => e = 0);
  });
};

const updatedRow  = arr => _.map(arr, e => e = 1);

const updEditable = (type, matrix, index) => {

  if (matrix.length > 0) {
    if (type === 'row') {
      return [
        ...matrix.slice(0, index),
        updatedRow(...matrix.slice(index, index + 1)),
        ...matrix.slice(index + 1)
      ];
    }

    if (type === 'column') {
      return _.map(matrix, a => {
        return _.map(a, (e, i) => {
          if (i !== index + 1) {
            return e;
          }
          return e = 1;
        });
      });
    }
  }

  return;
}

const newMatrix = (type, matrix) => {
  if (matrix.length > 0) {
    if (type === 'row') {
      // console.log(matrix.slice(matrix.length - 1))
      return [
        ...matrix,
        updatedRow(...matrix.slice(matrix.length - 1))
      ]
    }

    if (type === 'column') {
      return _.map(matrix, (a) => {
        return [...a, 1]//_.concat(a, 1);
        // return [...a.concat(1)];
      });
    }
  } else {
    let newEditMatrix = [1, 1];
    return [
      newEditMatrix
    ]
  }

  return;
};

const editCell = (arr, rid, cid) => {
  return _.map(arr, (r, i) =>  {
    if (rid === i) {
      return  _.map(r, (item, j) => {
        if (j !== cid) {
          return item;
        }
        return item = 1;
      });

    }
    return r;
  });
}

const removeNew = (arr, type) => {

  if (type === 'row') {
    return [
      ...arr.slice(0, arr.length - 1)
    ];
  }

  if (type === 'column') {
    return _.map(arr, (r, i) => r.slice(0, r.length - 1));
  }
  return;
}

export default function reducer(state=init, action) {

  switch (action.type) {

    case 'CREATE_EDIT_MATRIX': {

      let em = [];
      let rows = action.payload;

      if (rows.length > 0) {
        let cols = 1 + action.payload[0].SizeList.length;

        for (let i = 0; i < rows.length; i += 1) {
          em[i] = [];
          for (let j = 0; j < cols; j += 1) {
            em[i][j] = 0;
          }
        }
      }

      return {
        ...state,
        editMatrix: em
      };
    }

    case 'EDIT_ROW': {
      return {
        ...state,
        editing: true,
        editingA: 'row',
        editingId: action.payload,
        editMatrix: updEditable('row', state.editMatrix, action.payload)
      };
    }

    case 'ADD_ROW': {
      return {
        ...state,
        editing: true,
        editingA: 'row',
        editingId: state.editMatrix.length,
        editMatrix: newMatrix('row', state.editMatrix)
      }
    }

    case 'EDIT_COLUMN': {

      return {
        ...state,
        editing: true,
        editingA: 'column',
        editingId: action.payload,
        editMatrix: updEditable('column', state.editMatrix, action.payload)
      };
    }

    case 'ADD_COLUMN': {
      return {
        ...state,
        editing: true,
        editingA: 'column',
        saved: false,
        editingId: state.editMatrix[0].length - 1,
        editMatrix: newMatrix('column', state.editMatrix)
      };
    }

    case 'DISABLE_EDIT': {
      return {
        ...state,
        editing: false,
        editingA: '',
        editingId: null,
        saved: null,
        editMatrix: nullify(state.editMatrix),

      }
    }
    case 'DISABLE_EDIT_AND_REMOVE': {
      return {
        ...state,
        editing: false,
        editingA: '',
        editingId: null,
        saved: null,
        editMatrix: removeNew(state.editMatrix, action.payload)
      }
    }

    case 'SAVE_ROW': {
      return {
        ...state,
        editingA: action.payload
      };
    }

    case 'SET_EDITABLE': {
      return {
        ...state,
        data: [...state.data, action.payload]
      };
    }

    case 'SET_VIEW_TYPE': {
      return {
        ...state,
        viewType: action.payload
      }
    }

    case 'ENABLE_CELL_EDIT': {
      return {
        ...state,
        editing: true,
        editingA: 'cell',
        editMatrix: editCell(state.editMatrix, action.payload.rid, action.payload.cid)
      }
    }

    case 'NON_EMPTY_DATA': {
      return {
        ...state,
        editing: action.payload
      }
    }

    case 'EMPTY_DATA': {
      return {
        ...state,
        editing: action.payload,
        editingId: -1
      }
    }

    default: {
      return state;
    }
  }

};
