import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { reducer as notifications } from 'react-notification-system-redux';
// import { enableCellEdit } from '../actions/settingsActions';

import data from './dataReducer';
import settings from './settingsReducer';

export default combineReducers({
  data,
  settings,
  notifications,
  form: formReducer.plugin({
    'editSizes': (state, action) => {
      switch (action.type) {
        case 'DISABLE_EDIT_AND_REMOVE':
          return {
            ...state,
            values: undefined,
            registeredFields: undefined
          };
        // case 'ENABLE_CELL_EDIT': {
        //   console.log(state, action.payload);
        //   const { values } = action.payload;
        //   return {
        //     ...state,
        //     values
        //
        //   }
        // }
        default:
          return state;
      }
    }
  })
});
