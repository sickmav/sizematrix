import _ from 'lodash';

const init = {
  data: [],
  brands: [],
  allSizeTypes: [],
  sizesChart: [],
  regions: [],
  sizeTypeId: -1, //23,//null,
  brandId: -1,//0,//null,
  sizeTypeName: null,
  sizeTypeDescription: null,

  editing: null,
  editingId: -1,

  fetchingAll: false,
  fetchedAll: false,

  fetchingBrands: false,
  fetchedBrands: false,

  fetchingById: false,
  fetchedById: false,

  isNewData: false,
  saving: false,

  error: null,
  success: null,

  emptyRow: {
    SizeRegionName: 'region',

    SizeList: [{
      SizeId: 'si-1',
      ColumnIndex: 1,
      Size: 0
    }]
  }
};


// function updateObject(oldObject, newValues) {
//     // Encapsulate the idea of passing a new object as the first parameter
//     // to Object.assign to ensure we correctly copy data instead of mutating
//     return Object.assign({}, oldObject, newValues);
// }
//
// function updateItemInArray(array, itemId, updateItemCallback) {
//     const updatedItems = _.map(array, item => {
//         if(item.id !== itemId) {
//             // Since we only want to update one item, preserve all others as they are now
//             return item;
//         }
//
//         // Use the provided callback to create an updated item
//         const updatedItem = updateItemCallback(item);
//         return updatedItem;
//     });
//
//     return updatedItems;
// }

const aNew = (type, arr) => {
  if (type === 'row') {
    if (arr.length > 0) {

      let last = arr[arr.length - 1];
      const newSizeList = _.map(last.SizeList, (e, i) => {
        return { SizeId: i, Size: null };
      })
      const newRow = Object.assign({}, last, {
        ...last,
        SizeRegionId: null,
        SizeRegionName: null,
        SizeList: newSizeList
      });

      return [
      ...arr,
      newRow
      ];
    } else {
      const firstRow = {
        SizeRegionId: null,
        SizeList: [
          {
            Size: null,
            SizeId: -1
          }
        ]
      }
      return [
        firstRow
      ]
    }
    // ...arr.slice(arr.length - 1)
  }

  if (type === 'column') {
    return _.map(arr, (r, i) => {
      return {
        ...r,
        SizeList: [
          ...r.SizeList,
          {
            SizeId: i,
            ColumnIndex: r.SizeList.length,
            Size: null
          }
        ]
      };
    });

  }

  return;
}


const removeNew = (type, arr) => {

  if (type === 'row') {
    return [
      ...arr.slice(0, arr.length - 1)
    ];
  }

  if (type === 'column') {
    return _.map(arr, (r, i) => {
      return {
        ...r,
        SizeList: [
          ...r.SizeList.slice(0, r.SizeList.length - 1)
        ]
      }
    });
  }
  return;
}

export default function reducer(state=init, action) {

  switch (action.type) {
    case 'FETCH_ALL_SIZE_TYPES_STARTED': {
      return {
        ...state,
        fetchingAll: action.payload
      };
    }
    case 'FETCH_ALL_SIZE_TYPES_DONE': {
      return {
        ...state,
        fetchingAll: false,
        fetchedAll: true,
        allSizeTypes: JSON.parse(action.payload)
      };
    }
    case 'FETCH_ALL_SIZE_TYPES_ERR':  {
      return {
        ...state,
        fetchingAll: false,
        error: action.payload
      };
    }

    case 'FETCH_BRANDS_OF_SIZE_TYPE_STARTED': {
      return {
        ...state,
        fetchingBrands: action.payload
      }
    }
    case 'FETCH_BRANDS_OF_SIZE_TYPE_DONE': {
      return {
        ...state,
        fetchingBrands: false,
        fetchedBrands: true,
        brands: action.payload
      }
    }
    case 'FETCH_BRANDS_OF_SIZE_TYPE_ERR': {
      return {
        ...state,
        fetchingBrands: false,
        error: action.payload
      };
    }

    case 'FECTH_REGIONS_STARTED': {
      return {
        ...state
      }
    }
    case 'FETCH_REGIONS_DONE': {
      return {
        ...state,
        regions: action.payload
      }
    }
    case 'FETCH_REGIONS_ERR': {
      return {
        ...state,
        error: action.payload
      }
    }

    case 'FETCH_CHART_STARTED': {
      return {
        ...state,
        fetchingById: action.payload,
        // sizesChart: [],
        // fetchedById: false
      };
    }
    case 'FETCH_CHART_DONE': {
      return {
        ...state,
        fetchingById: false,
        fetchedById: true,
        sizesChart: action.payload
      };
    }
    case 'FETCH_CHART_ERR': {
      return {
        ...state,
        fetchingById: false,

        error: action.payload
      };
    }

    case 'SET_SIZE_TYPE': {
      return {
        ...state,
        sizeTypeId: action.payload.sizeTypeId,
        sizeTypeName: action.payload.sizeTypeName
      }
    }
    case 'SET_BRAND': {
      return {
        ...state,
        brandId: action.payload.brandId,
        brandName: action.payload.brandName
      }
    }
    case 'SET_CELL_ID': {
      return {
        ...state,
        editingId: action.payload //????
      }
    }

    case 'ADD_NEW_ROW': {
      return {
        ...state,
        isNewData: true,
        sizesChart: aNew(action.payload, state.sizesChart)
      };
    }
    case 'SAVE_ROW_STARTED': {
      return {
        ...state,
        saving: true,
        // fetchedById: false
      };
    }
    case 'SAVE_ROW_DONE': {
      return {
        ...state,
        saving: false
      };
    }
    case 'SAVE_ROW_ERR': {
      return {
        ...state,
        error: action.payload,
        saving: false
      };
    }

    case 'DELETE_ROW_STARTED': {
      return {
        ...state,
        saving: true
      };
    }
    case 'DELETE_ROW_DONE': {
      return {
        ...state,
        saving: false
      };
    }
    case 'DELETE_ROW_ERR': {
      return {
        ...state,
        error: action.payload,
        saving: false
      };
    }

    case 'ADD_NEW_COLUMN': {

      return {
        ...state,
        isNewData: true,
        sizesChart: aNew(action.payload, state.sizesChart)
      };
    }
    case 'SAVE_COLUMN_STARTED': {
      return {
        ...state,
        saving: true,
        // fetchedById: false
      };
    }
    case 'SAVE_COLUMN_DONE': {
      return {
        ...state,
        saving: false

      };
    }
    case 'SAVE_COLUMN_ERR': {
      return {
        ...state,
        error: action.payload,
        saving: false
      };
    }

    case 'DELETE_COLUMN_STARTED': {
      return {
        ...state,
        saving: true,
        // fetchedById: false
      };
    }
    case 'DELETE_COLUMN_DONE': {
      return {
        ...state,
        saving: false

      };
    }
    case 'DELETE_COLUMN_ERR': {
      return {
        ...state,
        error: action.payload,
        saving: false
      };
    }


    case 'REMOVE_UNSAVED_COLUMN':  {
      return {
        ...state,
        isNewData: false,
        sizesChart: removeNew(action.payload, state.sizesChart)
      }
    }

    case 'REMOVE_UNSAVED_ROW': {
      return {
        ...state,
        isNewData: false,
        sizesChart: removeNew(action.payload, state.sizesChart)
      }
    }

    case 'REMOVE_UNSAVED_CELL': {
      return {
        ...state,
        isNewData: false
      }
    }

    case 'CLEAR_DATA': {
      return {
        ...state,
        sizesChart: action.payload,
        sizeTypeId: -1,
        brandId: -1,
        editingA: ''
      }
    }

    ////////////////////////////////////
    case 'FETCH_BY_ID_STARTED': {
      return {
        ...state,
        fetchingById: action.payload,
        sizesChart: []
      };
    }
    case 'FETCH_BY_ID_DONE': {
      return {
        ...state,
        data: action.payload
      };
    }
    case 'FETCH_BY_ID_ERR': {
      return {
        ...state,
        error: action.payload
      };
    }

    case 'UPDATE_SIZES_STARTED': {
      return {
        ...state,
        saving: true
        // fetchedById: false
      };
    }
    case 'UPDATE_SIZES_DONE': {
      return {
        ...state,
        saving: false
      };
    }
    case 'UPDATE_SIZES_ERR': {
      return {
        ...state,
        error: action.payload,
        saving: false
      };
    }
    case 'UPDATE_CELL_STARTED': {
      return {
        ...state,
        saving: true
      }
    }
    case 'UPDATE_CELL_DONE': {
      return {
        ...state,
        saving: false
      }
    }
    case 'UPDATE_CELL_ERR': {
      return {
        ...state,
        saving: false
      }
    }
    ////////
    case 'ADD_NEW_SIZE_TYPE_STARTED': {
      return {
        ...state,
        sizeTypeId: -1
      };
    }
    case 'ADD_NEW_SIZE_TYPE_DONE': {
      return {
        ...state,
        sizeTypeId: action.payload.success,
        success: action.payload.message
      };
    }
    case 'ADD_NEW_SIZE_TYPE_ERR': {
      return {
        ...state,
        success: null,
        error: action.payload
      };
    }

    //////////
    case 'FETCH_BRANDS_STARTED': {
      return {
        ...state
      };
    }
    case 'FETCH_BRANDS_DONE': {
      return {
        ...state,
        brands: JSON.parse(action.payload)
      };
    }
    case 'FETCH_BRANDS_ERR': {
      return {
        ...state
      };
    }



    default: {
      return state;
    }
  }
};
