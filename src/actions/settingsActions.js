// export function save() {
//   return {
//     type: 'SAVE_ROW',
//     payload: null
//   };
// }
export const editRow = (rowIndex) => (dispatch) => {
  dispatch({
    type: 'EDIT_ROW',
    payload: rowIndex
  });
}

// export function addRow() {
//   return {
//     type: 'ADD_ROW',
//     payload: 'row'
//   }
// }
// export function addColumn() {
//   return {
//     type: 'ADD_COLUMN',
//     payload: 'column'
//   }
// }


export const editColumn = (columnIndex) => (dispatch) => {
  dispatch({
    type: 'EDIT_COLUMN',
    payload: columnIndex
  });
}

export const addNew = (type) => (dispatch) => {
  dispatch({
    type: 'ADD_' + type.toUpperCase(),
    payload: type
  });
}

export const disableEdit = () => (dispatch) => {
  dispatch({
    type: 'DISABLE_EDIT',
    payload: null
  });
}

export const disableEditAndRemove = (type) => (dispatch) => {
  dispatch({
    type: 'DISABLE_EDIT_AND_REMOVE',
    payload: type
  });
}

export const setViewType = (type) => (dispatch) => {
  if (type === '') {
    dispatch({
      type: 'SET_VIEW_TYPE',
      payload: null
    });
  } else {
    dispatch({
      type: 'SET_VIEW_TYPE',
      payload: type
    });
  }
}

export const enableCellEdit = (rid, cid, values) => (dispatch) => {
  dispatch({
    type: 'ENABLE_CELL_EDIT',
    payload: {
      rid,
      cid,
      values
    }
  });
}

export const saving = (type) => (dispatch) => {
    dispatch({
      type: 'SAVING',
      payload: type
    });
  }
