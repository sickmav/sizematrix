import axios from 'axios';
import Notifications from 'react-notification-system-redux';
import { disableEdit } from './settingsActions';

export const fetchAllSizeTypes = () => (dispatch) => {
  dispatch({
    type: 'FETCH_ALL_SIZE_TYPES_STARTED',
    payload: true
  });
  return axios({
    url: '/Sizes/GetAllSizeTypes',
    method: 'get'
  })
  .then((res) => {
    dispatch({
      type: 'FETCH_ALL_SIZE_TYPES_DONE',
      payload: res.data
    })
  })
  .catch((err) => {
    dispatch({
      type: 'FETCH_ALL_SIZE_TYPES_ERR',
      payload: err
    })
    throw err
  });
}

export const setSizeType = (sizeTypeId, sizeTypeName = null) => (dispatch) => {
  dispatch({
    type: 'SET_SIZE_TYPE',
    payload: {
      sizeTypeId,
      sizeTypeName
    }
  })
}

export const getBrandsListAssignedToSizeType = (sizeTypeId) => (dispatch) => {
  dispatch({
    type: 'FETCH_BRANDS_OF_SIZE_TYPE_STARTED',
    payload: true
  });

  return axios({
    url: '/Sizes/GetBrandsListAssignedToSizeType',
    method: 'get',
    params: {
      sizeTypeId
    }
  }).then((res) => {
    dispatch({
      type: 'FETCH_BRANDS_OF_SIZE_TYPE_DONE',
      payload: res.data
    });
    dispatch({
      type: 'SET_BRAND',
      payload: {
        brandId: 0,
        brandName: 'General'
      }
    });
  }).catch((err) => {
    dispatch({
      type: 'FETCH_BRANDS_OF_SIZE_TYPE_ERR',
      payload: err
    });
    throw err
  })
}

export const setBrand =(brandId, brandName) => (dispatch) => {
  dispatch({
    type: 'SET_BRAND',
    payload: {
      brandId,
      brandName
    }
  });
}

export const setCellId = (cellId) => (dispatch) => {
  dispatch({
    type: 'SET_CELL_ID',
    payload: cellId
  });
}

export const fetchChart = (sizeTypeId, brandId) => (dispatch) => {
  dispatch({
    type: 'FETCH_CHART_STARTED',
    payload: true
  });

  return axios({
    url: '/Sizes/GetSizesBySizeTypeId',
    method: 'get',
    params: {
      SizeTypeId: sizeTypeId,
      BrandId: brandId <= 0 ? 0: brandId
      // BrandId: brandId <= 0 ? null: brandId
    }
  }).then((res) => {

    return new Promise((resolve, reject) => {
      dispatch({
        type: 'FETCH_CHART_DONE',
        payload: JSON.parse(res.data)
      });

      resolve(res.data);
    })

  }).then(d => {
    dispatch({
      type: 'CREATE_EDIT_MATRIX',
      payload: JSON.parse(d)
    });

  }).catch((err) => {
    dispatch({
      type: 'FETCH_CHART_ERR',
      payload: err
    })
    throw err
  });
}


export const saveColumn = (sizeTypeId, columnIndex, sizes, brandId) => (dispatch) => {
  dispatch({
    type: 'SAVE_COLUMN_STARTED',
    payload: 'column'
  });

  return axios({
      url: '/Sizes/AddNewColumn',
      method: 'post',
      data:{
        sizeTypeId,
        columnIndex,
        sizes
      }
    }).then((res) => {
      if (res.data.success === true) {
        dispatch({
          type: 'SAVE_COLUMN_DONE',
          payload: res.data
        })
        dispatch(Notifications.success({
          message: res.data.message,
          position: 'tr',
          uid: 'saveColumnDone',
          autoDismiss: 3,
          dismissible: false
        }));
        return dispatch(fetchChart(sizeTypeId, brandId <= 0? null: brandId));
      } else {
        dispatch({
          type: 'SAVE_COLUMN_ERR',
          payload: res.data.message
        })
        dispatch(Notifications.error({
          message:  res.data.message,
          position: 'tr',
          uid: 'saveColumnErr',
          autoDismiss: 3,
          dismissible: false
        }));
      }
    }).catch((err) => {
      dispatch({
        type: 'SAVE_COLUMN_ERR',
        payload: err
      })
      throw err
    });

}

export const deleteColumn = (sizeTypeId, columnIndex, brandId) => (dispatch) => {
  dispatch({
    type: 'DELETE_COLUMN_STARTED',
    payload: 'column' // TODO ?
  })
  return axios({
    url: '/Sizes/DeleteColumn',
    method: 'post',
    data: {
      sizeTypeId,
      columnIndex,
      // brandId: brandId < 0 ? 0: brandId
      brandId: brandId <= 0 ? null: brandId
    }
  }).then((res) => {
    if (res.data.success === true) {
      dispatch({
        type: 'DELETE_COLUMN_DONE',
        payload: res.data
      })
      dispatch(Notifications.success({
        message: res.data.message,
        position: 'tr',
        uid: 'deleteColumnDone',
        autoDismiss: 3,
        dismissible: false
      }));
      return dispatch(fetchChart(sizeTypeId, (brandId <= 0? null: brandId)));
    } else {
      dispatch({
        type: 'DELETE_COLUMN_ERR',
        payload: res.data.message
      })
      dispatch(Notifications.error({
        message:  'This size is already used for products assigned to active offers! Deletion is not allowed!',
        position: 'tr',
        uid: 'deleteColumnErr',
        autoDismiss: 3,
        dismissible: false
      }));
    }
  }).catch((err) => {
    dispatch({
      type: 'DELETE_COLuMN_ERR',
      payload:  err
    })
    throw err
  })
}

export const saveRow = (sizeTypeId, sizeRegionId, sizes, brandId) => (dispatch) => {
  dispatch({
    type: 'SAVE_ROW_STARTED',
    payload: 'row'
  });

  return axios({
    url: '/Sizes/AddNewRow',
    method: 'post',
    data:{
      sizeTypeId,
      sizeRegionId,
      sizes,
      brandId: brandId < 0 ? null: brandId,
    }
  }).then((res) => {
    if (res.data.success === true) {
      dispatch({
        type: 'SAVE_ROW_DONE',
        payload: res.data
      })
      dispatch(Notifications.success({
        message: res.data.message,
        position: 'tr',
        uid: 'saveRowDone',
        autoDismiss: 3,
        dismissible: false
      }));
      return dispatch(fetchChart(sizeTypeId, brandId <= 0? null: brandId));
    } else {
      dispatch({
        type: 'SAVE_ROW_ERR',
        payload: res.data.message
      })
      dispatch(Notifications.error({
        message:  res.data.message,
        position: 'tr',
        uid: 'saveRowErr',
        autoDismiss: 3,
        dismissible: false
      }));
    }
  }).catch((err) => {
    dispatch({
      type: 'SAVE_ROW_ERR',
      payload: err
    })
    throw err
  })

}

export const deleteRow = (sizeTypeId, regionId, brandId) => (dispatch) => {
  dispatch({
    type: 'DELETE_ROW_STARTED',
    payload: 'column'
  });
  return axios({
    url: '/Sizes/DeleteRow',
    method: 'post',
    data: {
      sizeTypeId,
      regionId,
      brandId: brandId <= 0 ? null: brandId
    }
  }).then((res) => {

    if (res.data.success === true) {
      dispatch({
        type: 'DELETE_ROW_DONE',
        payload: res.data
      })
      dispatch(Notifications.success({
        message: res.data.message,
        position: 'tr',
        uid: 'deleteRowDone',
        autoDismiss: 3,
        dismissible: false
      }));
      return dispatch(fetchChart(sizeTypeId, (brandId <= 0? null: brandId)));
    } else {
      dispatch({
        type: 'DELETE_ROW_ERR',
        payload: res.data.message
      })
      dispatch(Notifications.error({
        message:  res.data.message,
        position: 'tr',
        uid: 'deleteRowErr',
        autoDismiss: 3,
        dismissible: false
      }));
    }
  }).catch((err) => {
    dispatch({
      type: 'DELETE_ROW_ERR',
      payload: err // TODO ?
    })
    throw err
  })
}

export const updateSizeCell = (sizeTypeId, size, brandId = null, regionId = null) => dispatch => {
    dispatch({
      type: 'UPDATE_CELL_STARTED',
      payload: 'cell'
    });

    return axios({
      url: '/Sizes/UpdateSizeCell',
      method: 'post',
      data: {
        model: size
      }
    }).then(res => {
      if (res.data.success === true ) {
        dispatch({
          type: 'UPDATE_CELL_DONE',
          payload: res.data
        });
        dispatch(Notifications.success({
          message: res.data.message,
          position: 'tr',
          uid: 'updateSizeDone',
          autoDismiss: 3,
          dismissible: false
        }));
        return dispatch(fetchChart(sizeTypeId, brandId));
      } else {
        dispatch(Notifications.error({
          message: res.data.message,
          position: 'tr',
          uid: 'updateSizeErr',
          autoDismiss: 3,
          dismissible: false
        }));
      }
    }).catch(err => {
      dispatch({
        type: 'UPDATE_CELL_ERR',
        payload: err
      })
      throw err
    });
  }

export const updateSizes = (sizeTypeId, sizes, brandId, regionId) => dispatch => {
  dispatch({
    type: 'UPDATE_SIZES_STARTED',
    payload: 'sizes'
  });

  return axios({
    url: '/Sizes/UpdateSizes',
    method: 'post',
    data: {
      sizeTypeId,
      sizes,
      brandId: brandId < 0 ? null: brandId,
      regionId
    }
  }).then(res => {
    if (res.data.success === true ) {
      dispatch({
        type: 'UPDATE_SIZES_DONE',
        payload: res.data
      });
      dispatch(Notifications.success({
        message: res.data.message,
        position: 'tr',
        uid: 'updateSizeDone',
        autoDismiss: 3,
        dismissible: false
      }));
      return dispatch(fetchChart(sizeTypeId, brandId));
    } else {
      dispatch(Notifications.error({
        message: res.data.message,
        position: 'tr',
        uid: 'updateSizeErr',
        autoDismiss: 3,
        dismissible: false
      }));
    }
  }).catch(err => {
    dispatch({
      type: 'UPDATE_SIZES_ERR',
      payload: err
    })
    throw err
  });
}

export const addNewData = (type) => (dispatch) => {
  dispatch({
    type: 'ADD_NEW_' + type.toUpperCase(),
    payload: type
  })
}

export const removeUnsavedData = (type) => (dispatch) => {
  dispatch({
    type: 'REMOVE_UNSAVED_' + type.toUpperCase(),
    payload: type,
  });
}

export const fetchRegions = () => (dispatch) => {
  dispatch({
    type: 'FECTH_REGIONS_STARTED',
    payload: true
  });


  return axios({
    url: '/Sizes/GetRegions',
    method: 'get'
  }).then(res => {
    dispatch({
      type: 'FETCH_REGIONS_DONE',
      payload: res.data
    })
  }).catch(err => {
    dispatch({
      type: 'FETCH_REGIONS_ERR',
      payload: err
    })
  })
}

export const addNewSizeType = (name, description) => (dispatch) => {
  dispatch({
    type: 'ADD_NEW_SIZE_TYPE_STARTED',
    payload: ''
  })
  return axios.post('/Sizes/AddNewSizeType', {
    sizeTypeName: name,
    sizeTypeDescription: description? description: ''
  })
  .then((res) => {
    if (res.data.success && res.data.success > 0) {
      dispatch({
        type: 'ADD_NEW_SIZE_TYPE_DONE',
        payload: res.data
      })

      dispatch(Notifications.success({
        message: res.data.message,
        position: 'tr',
        uid: 'addColumnDone',
        autoDismiss: 3,
        dismissible: false
      }));
    } else {
      dispatch(Notifications.error({
        message: res.data.message,
        position: 'tr',
        uid: 'addColumnErr',
        autoDismiss: 3,
        dismissible: false
      }));
    }
  }).catch((err) => {
    dispatch({
      type: 'ADD_NEW_SIZE_TYPE_ERR',
      payload: err
    });
  });
}

export const fetchBrands = (id) => (dispatch) => {
  dispatch({
    type: 'FETCH_BRANDS_STARTED',
    payload: ''
  });

  return axios({
    url: '/Sizes/GetAllBrands',
    method: 'get',
    params: {
      sizeTypeId: id
    }
  })
  .then((res) => {
    dispatch({
      type: 'FETCH_BRANDS_DONE',
      payload: res.data
    });
  })
  .catch((err) => {
    dispatch({
      type: 'FETCH_BRANDS_ERR',
      payload: err
    });
  });
}

export const fetchById = (id) => (dispatch) => {
  dispatch({
    type: 'FETCH_BY_ID_STARTED',
    payload: true
  });
  return axios({
    url: '/Sizes/GetSizesBySizeTypeId',
    method: 'get',
    params: {
      sizeTypeId: id
    }
  })
    .then((res) => {
      let data = JSON.parse(res.data);
      if (data.length === 0) {
        dispatch({
          type: 'EMPTY_DATA',
          payload: 'row'
        })
      } else if (data.length > 0) {
        dispatch({
          type: 'DISABLE_EDIT',
          payload: null
        })
      }
      dispatch({
        type: 'FETCH_BY_ID_DONE',
        payload: data
      });
    })
    .catch((err) => {
      dispatch({
        type: 'FETCH_BY_ID_ERR',
        payload: err
      });
    });
}

export const clearData = () => (dispatch) => {
  dispatch({
    type: 'CLEAR_DATA',
    payload: []
  });

  return dispatch(disableEdit())
}

  // export const addRow = (params) => {
  //   return function (dispatch) {
  //
  //     dispatch({
  //       type: 'ADD_ROW_STARTED',
  //       fetchingAll: true
  //     });
  //
  //     return axios({
  //       url: '/Sizes/AddNewRow',
  //       method: 'post',
  //       data: {
  //         sizeTypeId: params.sizeTypeId,
  //         sizeRegionId: params.sizeRegionId,
  //         sizes: params.sizes,
  //         sizeRegionName: params.sizeRegionName,
  //         brandId: params.brandId < 0 ? 0: params.brandId
  //       }
  //     }).then((res) => {
  //         dispatch({
  //           type: 'ADD_ROW_DONE',
  //           payload: res.data
  //         })
  //     }).catch((err) => {
  //
  //       dispatch({
  //         type: 'ADD_ROW_ERR',
  //         payload: err
  //       });
  //     });
  //   };
  // }
  //
  // export const addColumn = (params) => {
  //   return function (dispatch) {
  //
  //     dispatch({
  //       type: 'ADD_COLUMN_STARTED',
  //       fetchingAll: true
  //     });
  //
  //     return axios({
  //       url: '/Sizes/AddNewColumn',
  //       method: 'post',
  //       data: {
  //         sizeTypeId: params.sizeTypeId,
  //         columnIndex: params.columnIndex,
  //         sizes: params.sizes
  //       }
  //     }).then((res) => {
  //         dispatch({
  //           type: 'ADD_COLUMN_DONE',
  //           payload: res.data
  //         })
  //     }).catch((err) => {
  //
  //       dispatch({
  //         type: 'ADD_COLUMN_ERR',
  //         payload: err
  //       });
  //     });
  //   };
  // }
